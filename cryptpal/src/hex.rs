use std::ops::{Deref, DerefMut};
use thiserror::Error;

#[derive(Debug, PartialEq)]
pub struct Hex(Vec<u8>);

#[derive(Copy, Clone, Error, Debug, PartialEq)]
pub enum HexConversionError {
    #[error("found invalid hex character \"{0}\" during conversion")]
    InvalidHexCharacter(char),
    #[error("hex string has an odd length")]
    OddLength,
}

#[derive(Copy, Clone, Error, Debug, PartialEq)]
pub enum HexOperationError {
    #[error("hex instances are not of equal length")]
    LengthMismatch,
}

impl Hex {
    pub fn new(bytes: &[u8]) -> Self {
        Self(bytes.to_vec())
    }

    pub fn xor_single_byte(&self, byte: u8) -> Self {
        Self(self.iter().map(|a| a ^ byte).collect())
    }

    pub fn xor(&self, rhs: &Self) -> Result<Self, HexOperationError> {
        use HexOperationError::LengthMismatch;

        if self.len() != rhs.len() {
            return Err(LengthMismatch);
        }

        Ok(Self(
            self.iter().zip(rhs.iter()).map(|(a, b)| a ^ b).collect(),
        ))
    }

    pub fn decode_str(&self) -> String {
        self.iter().map(|byte| char::from(*byte)).collect()
    }
}

impl TryFrom<&str> for Hex {
    type Error = HexConversionError;

    fn try_from(val: &str) -> Result<Self, Self::Error> {
        use HexConversionError::*;

        let str_len = val.len();

        // Early exit if odd number of characters
        if str_len & 1 == 1 {
            return Err(OddLength);
        }

        // Construct an iterator that gives us a tuple of 2 characters at a time.
        let chunk_iter = val.chars().step_by(2).zip(val.chars().skip(1).step_by(2));

        // Construct a vector of conversion results from earlier iterator
        let result_vector = chunk_iter
            .map(|(a, b)| match (a.to_digit(16), b.to_digit(16)) {
                (Some(a_val), Some(b_val)) => Ok((a_val << 4 | b_val) as u8),
                (None, _) => Err(InvalidHexCharacter(a)),
                (_, None) => Err(InvalidHexCharacter(b)),
            })
            .collect::<Vec<Result<u8, HexConversionError>>>();

        // If some error occurred in conversion, return the first-found error.
        if let Some(Err(e)) = result_vector.iter().copied().find(|e| e.is_err()) {
            return Err(e);
        }

        // Map vec<result> into vec<u8> and return Ok(..)
        Ok(Self(
            result_vector.into_iter().filter_map(|r| r.ok()).collect(),
        ))
    }
}

impl Deref for Hex {
    type Target = Vec<u8>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Hex {
    fn deref_mut(&mut self) -> &mut <Self as Deref>::Target {
        &mut self.0
    }
}

impl ToString for Hex {
    fn to_string(&self) -> String {
        self.iter().map(|byte| format!("{byte:02X}")).collect()
    }
}

#[cfg(test)]
mod tests {
    use crate::hex::{Hex, HexConversionError};

    #[test]
    fn try_from_str_works() {
        let res = Hex::try_from("49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d");
        assert!(res.is_ok());
        let hex_val = res.unwrap();
        assert_eq!(
            hex_val,
            Hex::new(&[
                0x49, 0x27, 0x6d, 0x20, 0x6b, 0x69, 0x6c, 0x6c, 0x69, 0x6e, 0x67, 0x20, 0x79, 0x6f,
                0x75, 0x72, 0x20, 0x62, 0x72, 0x61, 0x69, 0x6e, 0x20, 0x6c, 0x69, 0x6b, 0x65, 0x20,
                0x61, 0x20, 0x70, 0x6f, 0x69, 0x73, 0x6f, 0x6e, 0x6f, 0x75, 0x73, 0x20, 0x6d, 0x75,
                0x73, 0x68, 0x72, 0x6f, 0x6f, 0x6d,
            ])
        );
    }

    #[test]
    fn to_string_works() {
        let hex = Hex::new(&[0x15, 0x2A, 0x54]);
        let hex_string = hex.to_string();
        assert_eq!(hex_string.as_str(), "152A54");
    }

    #[test]
    fn xor_works() {
        let hex_a = Hex::try_from("1c0111001f010100061a024b53535009181c").unwrap();
        let hex_b = Hex::try_from("686974207468652062756c6c277320657965").unwrap();

        let expected = Hex::try_from("746865206b696420646f6e277420706c6179").unwrap();

        assert_eq!(hex_a.xor(&hex_b).unwrap(), expected);
    }

    #[test]
    fn decode_str_works() {
        let hex = Hex::new(&[
            0x48, 0x65, 0x6c, 0x6c, 0x6f, 0x2C, 0x20, 0x57, 0x6f, 0x72, 0x6c, 0x64, 0x21,
        ]);
        assert_eq!(hex.decode_str().as_str(), "Hello, World!");
    }

    #[test]
    fn single_hex_byte() {
        let r = Hex::try_from("2A");
        assert!(r.is_ok());
        let hex_val = r.unwrap();
        assert_eq!(hex_val, Hex::new(&[0x2A]));
        assert_eq!(hex_val.len(), 1);
    }

    #[test]
    fn odd_hex_length() {
        let r = Hex::try_from("A");
        assert!(r.is_err());
        let err = r.unwrap_err();
        assert_eq!(err, HexConversionError::OddLength);
    }

    #[test]
    fn invalid_hex_character() {
        let r = Hex::try_from("2A4X");
        assert!(r.is_err());
        let err = r.unwrap_err();
        assert_eq!(err, HexConversionError::InvalidHexCharacter('X'));
    }
}
