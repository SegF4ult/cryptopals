const ALPHABET: &str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
const BIT_MASK: u32 = 0x3F;

#[derive(Debug, Clone, PartialEq)]
pub struct Base64(String);

impl Base64 {
    pub fn encode_bytes(input: &[u8]) -> String {
        input
            .chunks(3)
            .map(|bytes| {
                let mut chunk_res = String::default();
                let mut bitstr= 0_u32;
                let mut shift = 16;

                for v in bytes.iter() {
                    bitstr |= (*v as u32) << shift;
                    shift -= 8;
                }

                shift = 18;
                for _ in 0..=bytes.len() {
                    let v = (bitstr >> shift) & BIT_MASK;
                    shift -= 6;
                    chunk_res.push(ALPHABET.chars().nth(v as usize).unwrap());
                }
                while chunk_res.len() < 4 {
                    chunk_res.push('=');
                }

                chunk_res
            })
            .collect()
    }
}

#[cfg(test)]
mod tests {
    use super::Base64;
    use crate::hex::*;

    #[test]
    fn it_works() {
        // Set 1 - Challenge 1
        let hex_val = Hex::try_from("49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d").unwrap();
        assert_eq!(
            Base64::encode_bytes(&hex_val),
            String::from("SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t")
        );
    }

    #[test]
    fn three_input_octets() {
        let hex_val = Hex::try_from("4d616e").unwrap();
        assert_eq!(Base64::encode_bytes(&hex_val), String::from("TWFu"));
    }

    #[test]
    fn two_input_octets() {
        let hex_val = Hex::try_from("4d61").unwrap();
        assert_eq!(Base64::encode_bytes(&hex_val), String::from("TWE="));
    }

    #[test]
    fn one_input_octet() {
        let hex_val = Hex::try_from("4d").unwrap();
        assert_eq!(Base64::encode_bytes(&hex_val), String::from("TQ=="));
    }
}
