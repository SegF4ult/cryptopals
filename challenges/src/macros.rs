
macro_rules! challenge_eq {
    ($a:expr, $b:expr) => {
        if $a == $b {
            Ok(())
        } else {
            Err(anyhow!("desired equality does not hold"))
        }
    };
}

macro_rules! evaluate_challenge {
    ($f:ident) => {
        println!(concat!("[", stringify!($f), "]"));
        match $f() {
            Ok(_) => println!("result: OK"),
            Err(e) => println!("result: FAIL {:?}", e),
        }
        println!("");
    };
}
