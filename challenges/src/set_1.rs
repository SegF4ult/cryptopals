use anyhow::{anyhow, Result};
use cryptpal::{base64::*, hex::*};

pub fn run() {
    println!("=== Evaluating CryptoPals Set 1 ===");

    evaluate_challenge!(challenge_1);
    evaluate_challenge!(challenge_2);
    evaluate_challenge!(challenge_3);
    evaluate_challenge!(challenge_4);
    evaluate_challenge!(challenge_5);
    evaluate_challenge!(challenge_6);
    evaluate_challenge!(challenge_7);
    evaluate_challenge!(challenge_8);
}

fn challenge_1() -> Result<()> {
    let hex = Hex::try_from("49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d")?;
    let base64 = Base64::encode_bytes(&hex);

    challenge_eq!(
        base64.as_str(),
        "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"
    )
}

fn challenge_2() -> Result<()> {
    let hex_a = Hex::try_from("1c0111001f010100061a024b53535009181c")?;
    let hex_b = Hex::try_from("686974207468652062756c6c277320657965")?;

    let hex_expected = Hex::try_from("746865206b696420646f6e277420706c6179")?;

    challenge_eq!(hex_a.xor(&hex_b)?, hex_expected)
}

fn challenge_3() -> Result<()> {
    let hex_val =
        Hex::try_from("1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736")?;

    let mut key_candidate: u8 = 1;
    let mut best_alphabetic_count: usize = 0;

    for key in 1..=0xFF_u8 {
        let hex_xorred = hex_val.xor_single_byte(key);
        let alphabetic_chars = hex_xorred
            .iter()
            .filter(|byte| byte.is_ascii_alphabetic())
            .count();

        if alphabetic_chars > best_alphabetic_count {
            best_alphabetic_count = alphabetic_chars;
            key_candidate = key;
        }
    }

    println!(
        "[{key_candidate:#02X}]: {}",
        hex_val.xor_single_byte(key_candidate).decode_str()
    );
    Ok(())
}

fn challenge_4() -> Result<()> {
    Err(anyhow!("not implemented"))
}
fn challenge_5() -> Result<()> {
    Err(anyhow!("not implemented"))
}
fn challenge_6() -> Result<()> {
    Err(anyhow!("not implemented"))
}
fn challenge_7() -> Result<()> {
    Err(anyhow!("not implemented"))
}
fn challenge_8() -> Result<()> {
    Err(anyhow!("not implemented"))
}
